package net.azurewebsites.healthcareapp.client.healthcare.services

import android.content.Context
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.app.NotificationManager
import android.app.NotificationChannel
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.app.PendingIntent
import android.content.Intent


/*class MockingJService: FirebaseMessagingService() {

    val TAG = MockingJService::class.java.name

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: " + token!!)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        // sendRegistrationToServer(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage?.from);

        // Check if message contains a data payload.
        if (remoteMessage?.data?.size ?: 0 > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage?.data)

            if (/* Check if data needs to be processed by long running job */ false) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
              //  scheduleJob();
            } else {
                // Handle message within 10 seconds
                //handleNow();
            }

        }

        // Check if message contains a notification payload.
        remoteMessage?.notification?.apply {
            Log.d(TAG, "Message Notification Body: $body")
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        val pendingIntent = initPendingIntent()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationId = Random().nextInt(60000)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val channelId = getString(R.string.app_name)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels(notificationManager, channelId)

        }

        val  notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)//a resource for your custom small icon
                .setContentTitle(remoteMessage?.data?.get("title")) //the "title" value you sent in your notification
                .setContentText(remoteMessage?.getData()?.get("message")) //ditto
                .setAutoCancel(true)  //dismisses the notification on click
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        notificationManager.notify(notificationId, notificationBuilder.build())

    }

    private fun initPendingIntent(): PendingIntent {
        val notificationIntent = Intent(this, MainActivity::class.java)
       /* if (StartActivity.isAppRunning) {
            notificationIntent = Intent(this, MainActivity::class.java)
        } else {
            notificationIntent = Intent(this, MainActivity::class.java)
        }*/

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        return PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager, adminChannelId: String) {
        val adminChannelName = getString(R.string.app_name)
        val adminChannelDescription = getString(R.string.app_name)

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(adminChannelId, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)

        notificationManager.createNotificationChannel(adminChannel)
    }

}*/