package net.azurewebsites.healthcareapp.client.healthcare.utils.extensions

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by FAMILY on 9/23/2018.
 */
fun String.convertApiDateToDisplayDateTime(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    val apiDate = dateFormat.parse(this)

    val displayFormat = SimpleDateFormat("MMMM dd, yyyy hh:mm:ss aaa", Locale.getDefault())

    return displayFormat.format(apiDate)

}