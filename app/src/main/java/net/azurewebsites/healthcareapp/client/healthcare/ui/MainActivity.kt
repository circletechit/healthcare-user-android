package net.azurewebsites.healthcareapp.client.healthcare.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import net.azurewebsites.healthcareapp.client.healthcare.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
