package net.azurewebsites.healthcareapp.client.healthcare.utils.extensions

import android.content.Context
import android.support.v7.app.AlertDialog

/**
 * Created by FAMILY on 9/23/2018.
 */
fun Context.showAlert(
        title: String,
        message: String,
        action: (() -> Unit)? = null
) {
    AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                action?.invoke()
            }
            .create()
            .show()
}

fun Context.showConfirmationDialog(
        title: String,
        message: String,
        positiveTitle: Int = android.R.string.ok,
        negativeTitle: Int = android.R.string.cancel,
        cancelable: Boolean = false,
        positiveAction: (() -> Unit)? = null,
        negativeAction: (() -> Unit)? = null
) {
    AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveTitle) { _, _ ->
                positiveAction?.invoke()
            }
            .setNegativeButton(negativeTitle) {_,_ ->
                negativeAction?.invoke()

            }
            .setCancelable(cancelable)
            .create()
            .show()
}