package net.azurewebsites.healthcareapp.client.healthcare.utils.extensions

import android.content.Context
import android.content.Intent

/**
 * Created by FAMILY on 9/23/2018.
 */
fun Context.toNewActivity(newActivity: Class<*>) {
    val intent = Intent(this, newActivity)
    startActivity(intent)
}