package net.azurewebsites.healthcareapp.client.healthcare.customview

import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.bumptech.glide.Glide.init
import kotlinx.android.synthetic.main.custom_health_care_edit_text.view.*
import net.azurewebsites.healthcareapp.client.healthcare.R

class HealthCareEditText: RelativeLayout {

    private var mInflater: LayoutInflater
    private lateinit var view: View

    constructor(context: Context) : super(context) {
        mInflater = LayoutInflater.from(context)
        init()

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mInflater = LayoutInflater.from(context)
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mInflater = LayoutInflater.from(context)
        init(attrs)
    }

    private fun init(attrs: AttributeSet? = null) {
        view = mInflater.inflate(R.layout.custom_health_care_edit_text, this, true)
        attrs?.let { attribute ->
            context.theme.obtainStyledAttributes(
                attribute,
                R.styleable.HealthCareEditText,
                0, 0).apply {
                try {
                    val drawableLeft= getDrawable(R.styleable.HealthCareEditText_drawableLeft)
                    drawableLeft?.let {
                        view.edtField.setCompoundDrawablesWithIntrinsicBounds(it,null,null,null)
                        view.edtField.compoundDrawablePadding = context.resources.getDimension(R.dimen.dp_8).toInt()
                    }

                    val inputType = getInt(R.styleable.HealthCareEditText_android_inputType, InputType.TYPE_CLASS_TEXT)
                    view.edtField.inputType = inputType
                    /*if (inputType == TYPE_PASSWORD) {
                        view.appCompatEdtText.typeface =  Typeface.createFromAsset(context.assets,
                            "fonts/ubuntu_medium.ttf")
                        view.appCompatEdtText.transformationMethod = PasswordTransformationMethod()
                    }*/

                    val hint = getText(R.styleable.HealthCareEditText_android_hint)
                    view.edtField.hint = hint

                    val focus = getBoolean(R.styleable.HealthCareEditText_android_focusable, true)
                    val focusableInTouchMode = getBoolean(R.styleable.HealthCareEditText_android_focusableInTouchMode, true)
                    view.edtField.isFocusable = focus
                    view.edtField.isFocusableInTouchMode = focusableInTouchMode

                } finally {
                    recycle()
                }
            }

            edtField.setOnFocusChangeListener { _, hasFocus ->

                container.background =  if (hasFocus) {
                    ContextCompat.getDrawable(context, R.drawable.rounded_corner_edit_text_active)
                } else {
                    ContextCompat.getDrawable(context, R.drawable.rounded_corner_edit_text)
                }

            }
        }
    }

    companion object {
        const val TYPE_PASSWORD = 129;
    }
}