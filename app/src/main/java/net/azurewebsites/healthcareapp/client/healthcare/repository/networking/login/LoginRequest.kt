package com.circletechit.mocking.repository.networking.login

import com.google.gson.annotations.SerializedName

/**
 * Created by FAMILY on 9/23/2018.
 */
data class LoginRequest(
        @SerializedName("username")
        val username: String,

        @SerializedName("password")
        val password: String,

        @SerializedName("client_id")
        val clientId: String,

        @SerializedName("client_secret")
        val clientSecret: String,

        @SerializedName("grant_type")
        val grantType: String
)