package net.azurewebsites.healthcareapp.client.healthcare.constants

/**
 * Created by FAMILY on 9/23/2018.
 */
object Constants {

    const val REALM_SCHEMA_NAME = "mockingj.realm"

    const val BASE_URL = "base_url"
}