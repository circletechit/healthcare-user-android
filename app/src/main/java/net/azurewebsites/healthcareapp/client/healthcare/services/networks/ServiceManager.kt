package net.azurewebsites.healthcareapp.client.healthcare.services.networks

import android.content.Context
import android.content.ContextWrapper
import android.net.NetworkInfo
import android.net.ConnectivityManager



class ServiceManager(context: Context): ContextWrapper(context) {

    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}