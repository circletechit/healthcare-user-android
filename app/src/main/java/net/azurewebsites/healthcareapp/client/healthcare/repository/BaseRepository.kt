package net.azurewebsites.healthcareapp.client.healthcare.repository

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import net.azurewebsites.healthcareapp.client.healthcare.ui.HealthCareApplication.Companion.prefs
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

/**
 * Created by FAMILY on 9/23/2018.
 */
open class BaseRepository<Interface : Any>(clazz: Class<Interface>, baseUrl: String) {
    val gson: Gson = GsonBuilder().create()

    val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(initClient().build())
            .build()

    var repositoryApi: Interface = retrofit.create(clazz)

    private fun initClient(): OkHttpClient.Builder {
        val client = OkHttpClient.Builder().addInterceptor { chain ->

            val request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", "Bearer ${prefs.accesToken}").build()
            chain.proceed(request)
        }
                .authenticator { _, response ->
                    //https://medium.com/@techexe/retrofit-interceptor-add-authorization-token-c1e9a3035de7
                    if (responseCount(response) >= 2) {
                        // If both the original call and the call with refreshed token failed
                        // it will probably keep failing, so don't try again
                        //logout user
                       return@authenticator  null
                    }

                    // We need a new client, since we don't want to make another call using our client with access token
                    val  newAccessToken =  TokenRepository().refreshTokenSync()
                    if (newAccessToken != null) {
                        response.request()
                                .newBuilder()
                                .header("Authorization","Bearer $newAccessToken").build()
                    } else {
                        null
                    }

                }
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)



        /*if (BuildConfig.enableHttpLogging) {
            client.addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }*/

        return client
    }

    private fun responseCount(response: Response): Int {
        var result = 1
        while (response.priorResponse() != null) {
            result++
        }
        return result
    }

    fun checkErrorResponse(e: Throwable): String {
        return when (e) {
            is HttpException -> {
                val responseBody = (e as HttpException).response().errorBody()
                getSimpleErrorMessage(responseBody)
            }
            is SocketTimeoutException -> "Time Out "//timeout
            is IOException -> "No Internet Connection"
            else -> "Something Went Wrong(Server Error)"
        }
    }

    private fun getSimpleErrorMessage(responseBody: ResponseBody?): String {
        return try {
            val jsonObject = JSONObject(responseBody?.string())
            jsonObject.getString("message")
        } catch (e: Exception) {
            e.message.toString()
        }

    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}