package com.circletechit.mocking.repository.networking.token

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface TokenApi {

    @GET("users/{user}/repos")
    fun refreshToken(@Path("user") user: String): Call<String>
}