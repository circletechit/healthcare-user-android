package net.azurewebsites.healthcareapp.client.healthcare.utils

import android.content.Context

/**
 * Created by FAMILY on 9/23/2018.
 */
class BasePreferences(context: Context) {

    private val ACCESS_TOKEN_KEY = "access_token_key"
    private val REFRESH_TOKEN_KEY = "refresh_token_key"
    private val USER_INFO_KEY = "user_info_key"
    private val HAS_LOGIN = "has_login_key"
    private val HAS_FIRST_LAUNCH = "has_first_launch"
    private val preferences = context.getSharedPreferences("io.circletech.barangay.prefs", 0)

    var accesToken: String?
        get() = preferences.getString(ACCESS_TOKEN_KEY, null)
        set(value) = preferences.edit().putString(ACCESS_TOKEN_KEY, value).apply()

    var refreshToken: String?
        get() = preferences.getString(REFRESH_TOKEN_KEY, null)
        set(value) = preferences.edit().putString(REFRESH_TOKEN_KEY, value).apply()

    var hasLogin: Boolean
        get() = preferences.getBoolean(HAS_LOGIN, false)
        set(value) = preferences.edit().putBoolean(HAS_LOGIN, value).apply()

    var hasFirstLaunch: Boolean
        get() = preferences.getBoolean(HAS_FIRST_LAUNCH, true)
        set(value) = preferences.edit().putBoolean(HAS_FIRST_LAUNCH, value).apply()
    var userInfo: String
        get() = preferences.getString(USER_INFO_KEY, null)
        set(value) = preferences.edit().putString(USER_INFO_KEY, value).apply()

    fun clearAccessToken() {
        preferences.edit().remove(ACCESS_TOKEN_KEY).apply()
        preferences.edit().remove(REFRESH_TOKEN_KEY).apply()
        preferences.edit().remove(HAS_LOGIN).apply()
        preferences.edit().remove(USER_INFO_KEY).apply()
    }

}