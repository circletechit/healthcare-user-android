package net.azurewebsites.healthcareapp.client.healthcare.repository

import com.circletechit.mocking.repository.networking.login.LoginApi
import com.circletechit.mocking.repository.networking.login.LoginRequest
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.AsyncSubject
import net.azurewebsites.healthcareapp.client.healthcare.constants.Endpoints
import net.azurewebsites.healthcareapp.client.healthcare.repository.BaseRepository
import net.azurewebsites.healthcareapp.client.healthcare.ui.HealthCareApplication.Companion.prefs

/**
 * Created by FAMILY on 9/23/2018.
 */
class LoginRepository: BaseRepository<LoginApi>(LoginApi::class.java, Endpoints.BASE_URL) {

    fun login(username: String, password: String): AsyncSubject<Boolean> {
        val results: AsyncSubject<Boolean> = AsyncSubject.create()

        val loginRequest = LoginRequest(
                username = username,
                password = password,
                clientId = "",
                clientSecret = "",
                grantType = ""
        )

        repositoryApi.login(loginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { loginResponse->
                            prefs.accesToken = ""
                            prefs.refreshToken = ""
                            prefs.userInfo = Gson().toJson(loginResponse)
                            prefs.hasLogin = true

                            results.onNext(true)
                            results.onComplete()
                        },
                        onError = { results.onError(Throwable(checkErrorResponse(it))) }
                )
        return results
    }

}