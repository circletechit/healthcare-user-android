package net.azurewebsites.healthcareapp.client.healthcare.utils.extensions

import android.view.View

/**
 * Created by FAMILY on 9/23/2018.
 */
fun View.toVisible() {
    this.visibility = View.VISIBLE
}

fun View.toGone() {
    this.visibility = View.GONE
}

fun View.setVisibility(isVisible: Boolean) {
    this.visibility = if (isVisible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}