package com.circletechit.mocking.repository.networking.login

import io.reactivex.Observable
import net.azurewebsites.healthcareapp.client.healthcare.constants.Endpoints
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by FAMILY on 9/23/2018.
 */
interface LoginApi {

    @POST(Endpoints.LOGIN)
    fun login(@Body body: LoginRequest): Observable<Any>
}