package net.azurewebsites.healthcareapp.client.healthcare.constants

/**
 * Created by FAMILY on 9/23/2018.
 */
object Endpoints {

    const val BASE_URL = "https://api.barangay.circletechit.com"
    const val BASE_S3_URL = "https://circletechit.s3.ap-southeast-1.amazonaws.com"

    const val LOGIN = "/api/login"
}