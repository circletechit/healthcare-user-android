package net.azurewebsites.healthcareapp.client.healthcare.repository

import android.util.Log
import com.circletechit.mocking.repository.networking.token.TokenApi
import net.azurewebsites.healthcareapp.client.healthcare.constants.Constants
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import java.io.IOException


class TokenRepository {
    lateinit var tokenClient: TokenApi
    constructor() {
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        tokenClient = retrofit.create<TokenApi>(TokenApi::class.java)
    }

    fun refreshTokenSync(): String?  {
        var newAccessToken: String? = null
        val call =  tokenClient.refreshToken(""
                /*"grant_type",
                "client_id",
                "client_secret",
                "oauth_url",
                "refresh_token"*/);
        try {
            val  tokenResponse = call.execute();
            Log.e("refresh"," code"+tokenResponse.code());
            Log.e("request","url"+call.request().url());
            if(tokenResponse.code() == 200) {
                // save new access token
                newAccessToken = tokenResponse.body();
                //prefs.accesToken = newToken.accessToken
              //  prefs.refreshToken = newToken.refreshToken


            } else{
                //failed to get newAccess token user need to  logout
            }

        } catch(e: IOException) {
            e.printStackTrace()
        } finally {
            return newAccessToken
        }
    }
}