package net.azurewebsites.healthcareapp.client.healthcare.ui

import android.app.Application
import android.content.Context
import android.util.Log
import net.azurewebsites.healthcareapp.client.healthcare.utils.BasePreferences

class HealthCareApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext

        prefs = BasePreferences(this)

        Log.d("adsadad", "yow: " + prefs.accesToken)
        //Realm.init(this)

        /*val config = RealmConfiguration.Builder()
                .name(Constants.REALM_SCHEMA_NAME)
                .schemaVersion(0L)
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(config)*/
    }

    companion object {

        lateinit var appContext: Context
        lateinit var prefs: BasePreferences
    }
}