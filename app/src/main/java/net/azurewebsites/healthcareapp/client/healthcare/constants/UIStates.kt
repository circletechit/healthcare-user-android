package net.azurewebsites.healthcareapp.client.healthcare.constants

/**
 * Created by FAMILY on 9/23/2018.
 */
object UIStates {
    object UIStates {
        const val LOADING = "loading"
        const val LOADED = "loaded"
        const val ERROR = "error"
        const val EMPTY = "empty"
        const val EMPTY_LOADING = "empty_loading"
    }
}