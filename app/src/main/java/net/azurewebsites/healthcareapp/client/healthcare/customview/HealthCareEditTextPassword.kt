package net.azurewebsites.healthcareapp.client.healthcare.customview

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.custom_health_care_edit_text_password.view.*
import net.azurewebsites.healthcareapp.client.healthcare.R

class HealthCareEditTextPassword: RelativeLayout {

    private var mInflater: LayoutInflater
    private lateinit var view: View

    constructor(context: Context) : super(context) {
        mInflater = LayoutInflater.from(context)
        init()

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mInflater = LayoutInflater.from(context)
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mInflater = LayoutInflater.from(context)
        init(attrs)
    }

    private fun init(attrs: AttributeSet? = null) {
        view = mInflater.inflate(R.layout.custom_health_care_edit_text_password, this, true)
        attrs?.let { attribute ->
            context.theme.obtainStyledAttributes(
                attribute,
                R.styleable.HealthCareEditText,
                0, 0).apply {
                try {
                    val hint = getText(R.styleable.HealthCareEditText_android_hint)
                    view.tiePassword.hint = hint

                    val focus = getBoolean(R.styleable.HealthCareEditText_android_focusable, true)
                    val focusableInTouchMode = getBoolean(R.styleable.HealthCareEditText_android_focusableInTouchMode, true)
                    view.tiePassword.isFocusable = focus
                    view.tiePassword.isFocusableInTouchMode = focusableInTouchMode

                } finally {
                    recycle()
                }
            }

            tiePassword.setOnFocusChangeListener { _, hasFocus ->

                container.background =  if (hasFocus) {
                    ContextCompat.getDrawable(context, R.drawable.rounded_corner_edit_text_active)
                } else {
                    ContextCompat.getDrawable(context, R.drawable.rounded_corner_edit_text)
                }

            }
        }
    }

    companion object {
        const val TYPE_PASSWORD = 129;
    }
}