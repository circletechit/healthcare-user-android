package net.azurewebsites.healthcareapp.client.healthcare.utils.extensions

import java.text.NumberFormat

/**
 * Created by FAMILY on 9/23/2018.
 */
fun Int.formatToTwoDecimalPlaces(): String? {
    val formatter = NumberFormat.getInstance()
    formatter.maximumFractionDigits = 2
    formatter.minimumFractionDigits = 2

    return formatter.format(this)
}

fun Float.formatToTwoDecimalPlaces(): String? {
    val formatter = NumberFormat.getInstance()
    formatter.maximumFractionDigits = 2
    formatter.minimumFractionDigits = 2

    return formatter.format(this)
}

fun Double.formatToTwoDecimalPlaces(): String? {
    val formatter = NumberFormat.getInstance()
    formatter.maximumFractionDigits = 2
    formatter.minimumFractionDigits = 2

    return formatter.format(this)
}